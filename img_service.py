from flask import Flask, request, flash, url_for, redirect, render_template,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api
import datetime

# creating the flask app
app = Flask(__name__)
# creating an API object
api = Api(app)




app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/varadhi_smartek'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "random string"

db = SQLAlchemy(app)



class imgdata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, nullable=False)
    seller_id = db.Column(db.Integer, nullable=False)
    product_version_id = db.Column(db.Integer, nullable=False)
    image_name = db.Column(db.String(100))
    image_path = db.Column(db.String(100))
    created_on = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=datetime.datetime.utcnow)



class image_data(Resource):

    def get(self,id=1):
        page = id
        per_page = 10
        data = imgdata.query.filter_by().paginate(page,per_page,error_out=False)
        page_data= {}
        page_data['page_id'] = page
        page_data['per_page'] = per_page
        data_json = []
        for each in data.items:
            dict_data = {
                "id": each.id,
                "product id": each.product_id,
                "seller id": each.seller_id,
                "product version id": each.product_version_id,
                "image name": each.image_name,
                "image path": each.image_path,
                "created on": str(each.created_on),
                "updated on": str(each.updated_on)

            }
            data_json.append(dict_data)
        page_data['img_data'] = data_json
        return data_json, 200


        # Corresponds to POST request

    def post(self):
        data = request.get_json()
        #data = eval(data)
        print(data)
        print(type(data))

        imgdata_insert = []
        for img in data:
            new_entry = imgdata(product_id=img['product_id'],seller_id=img['seller_id'],product_version_id=img['product_version_id'],image_name=img['image_name'],image_path=img['image_path'])
            imgdata_insert.append(new_entry)
        db.session.add_all(imgdata_insert)
        db.session.commit()


        # request.form['createdby']

        # User(request.form['name'], request.form['city'],request.form['addr'], request.form['pin'])

        #db.session.add(Project(**record))
        #db.session.commit()
        res = {"message":"project saved"}
        return res,201

    def delete(self, id=None):
        if id:
            print(id)
            try:
                imgdata.query.filter(imgdata.id == id).delete()
            except Exception as e:
                print(e)
                return  "unable to delete data, error :" + e


        else:
            return "need to specify a project id"

api.add_resource(image_data, '/image_data','/image_data/<int:id>')


if __name__ == '__main__':
    db.create_all()
    app.run(host="0.0.0.0",port=9010)